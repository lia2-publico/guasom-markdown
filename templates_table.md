<div class="center-resize">
<h1 class="center-content">Template labels</h1>

<table class="tg">
<thead>
  <tr>
    <th class="tg-uzvj">Type</th>
    <th class="tg-uzvj">Basic label</th>
    <th class="tg-uzvj">Specific label</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-9wq8, border2" rowspan="10">Star</td>
    <td class="tg-9wq8, bb" rowspan="3">Star Early<br></td>
    <td class="tg-9wq8">Star O</td>
  </tr>
  <tr>
    <td class="tg-9wq8">Star B</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Star A</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb" rowspan="2">Star Intermediate</td>
    <td class="tg-9wq8">Star F</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Star G</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb" rowspan="2">Star Late</td>
    <td class="tg-9wq8">Star K</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Star M</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">White Dwarf</td>
    <td class="tg-9wq8, bb"></td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Emission Line Star</td>
    <td class="tg-9wq8, bb"></td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Carbon Star</td>
    <td class="tg-9wq8, bb"></td>
  </tr>
  <tr>
    <td class="tg-9wq8, border2" rowspan="6">Quasar</td>
    <td class="tg-9wq8, bb" rowspan="3">Quasar Z25_GT</td>
    <td class="tg-9wq8">Quasar Z45_GT</td>
  </tr>
  <tr>
    <td class="tg-9wq8">Quasar Z35_45</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Quasar Z25_35</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Quasar Z15_25</td>
    <td class="tg-9wq8, bb"></td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb" rowspan="2">Quasar Z15_LT</td>
    <td class="tg-9wq8">Quasar Z05_15</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Quasar Z05_LT</td>
  </tr>
  <tr>
    <td class="tg-9wq8, border2" rowspan="6">Galaxy</td>
    <td class="tg-9wq8, bb" rowspan="2">Galaxy Z02_GT</td>
    <td class="tg-9wq8">Galaxy Z025_GT</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Galaxy Z02_025</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb" rowspan="2">Galaxy Z01_02</td>
    <td class="tg-9wq8">Galaxy Z015_02</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Galaxy Z01_015</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb" rowspan="2">Galaxy Z01_LT</td>
    <td class="tg-9wq8">Galaxy Z005_01</td>
  </tr>
  <tr>
    <td class="tg-9wq8, bb">Galaxy Z005_LT</td>
  </tr>
</tbody>
</table>
</div>
