<div class="center-resize">
<h1 class="center-content">Gaia Utility for the Analysis of Self-Organizing Maps in DR3</h1>

For Gaia DR3 Outlier Analysis (OA) 56 million objects are analysed, with a probability of membership to typical astronomical object classes below a certain threshold (see DR3 [documentation](https://gea.esac.esa.int/archive/documentation/GDR3/Data_analysis/chap_cu8par/sec_cu8par_apsis/ssec_cu8par_apsis_oa.html)), i.e. classification outliers. Self-Organizing Maps (SOM) is the unsupervised clustering method selected to perform this task. To facilitate the analysis of the Gaia DR3 outliers map, a free access adaptive visualization tool has been developed, GUASOM flavour DR3, that allows to explore the relations between millions of objects of complex nature, with several oriented domain visualizations.
</div>


