<div class="center-resize">
<h1 class="center-content">Basic templates</h1>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_early.png" alt="Early Star"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_intermediate.png" 
alt="Intermediate Star"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_late.png" alt="Late Star"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_cs.png" alt="Carbon Star"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_els.png" alt="Emission Line Star"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_wd.png" alt="White Dwarf"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_qso_z15_25.png" alt="Quasar Z15_25"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_qso_z15_lt.png" alt="Quasar Z15_LT"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_gal_z01_02.png" alt="Galaxy Z01_02"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_gal_z01_lt.png" alt="Galaxy Z01_LT"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/template_gal_z02_gt.png" alt="Galaxy Z02_GT"/>
</div>
</div>
</div>
