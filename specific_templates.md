<div class="center-resize">
<h1 class="center-content">Specific templates</h1>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_a.png" alt="A Star"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_f.png" alt="F Star"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_g.png" alt="G Star"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_k.png" alt="K Star"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_m.png" alt="M Star"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_gal_z005_lt.png" alt="Galaxy Z005_LT"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_gal_z005_01.png" alt="Galaxy Z005_01"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_gal_z01_015.png" alt="Galaxy Z01_015"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_gal_z025_gt.png" alt="Galaxy Z25_GT"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_qso_z05_lt.png" alt="Quasar Z05_LT"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_qso_z05_15.png" alt="Quasar Z05_15"/>
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_qso_z25_35.png" alt="Quasar Z25_35"/>
</div>
</div>
<div class="image-side-group">
<div class="image-side">
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/specific_template_qso_z35_45.png" alt="Quasar Z35_45"/>
</div>
</div>
</div>
