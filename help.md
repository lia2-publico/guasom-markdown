<div class="center-resize">
<h1 class="center-content">Visualization tool user guide</h1>

<hr>
<br/>

An upper bar to navigate and view information on the current status of the application. The available elements are (left to right) 

- **Visualization**: Tab devoted to analyse Self-Organizing Maps by means of several visualizations.
- **Help**: It shows the description and the user's guide.
- **About Us**: It provides information about the organizations that provided support for this project.
- **Current map &gt; Plot**: Name of the map and the plot that is currently being visualized.
- **User name**: Name of the user if logged id.
- **Application version**: Current version of the tool.

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/GUASOM-toolbar.png" alt="toolbar"  class="center-resize"/>

<br/>
<hr>

<h2>Visualization</h2>

The left menu is devoted to map and plot selection:

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/select_som_plot.png" alt="selectSom"/>

First, the SOM map must be selected in the "SOM's" drop-down button then, the content of the "Plots" drop-down button is updated with the list of available plots for this map. To visualize any of them, simply choose the plot and then click in the chart type among the available ones.

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/GUASOM_Selectors.png" alt="selectFilter"/>

Chart types available (from left to right)

- Square map: It shows the SOM in a rectangular topology
- Hexagon map: It shows the SOM in an hexagonal topology
- 3-Dimensional map: It shows the SOM in 3 dimensions, combining the information of two types of plots
- Pie chart: It shows the classical pie chart representing the population of the map, This is only available for the global statistics

<cite>If the option is in dark gray means that it is not available and cannot be selected.</cite>

Some visualizations have drop-down menus or horizontal sliders to control some aspects of the view.

<hr>
<h3>Map</h3>

Most of the interface is devoted to represent the data of the SOM map and its legend.

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/quality.png" alt="quality_category plot"/>

Moving the cursor towards one of the hexagons, which represents a neuron, a legend with the information belonging to such neuron is shown.

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/neuron_tooltip.png" width="20%" alt="neuron tooltip"/>

<hr>
<h3>Neuron</h3>
It is possible to select any neuron by clicking on it (left click) and to explore its content with a double click.

To explore the content of a neuron some tabs are available, allowing to visualize the internal spectra, the Gaia spectra, the statistical summary, and, if available, the population.
- **Internal Spectra**: This tab shows the prototype and the object-centroid of the neuron, it also shows the spectra that was used to train the SOM, after the preprocess and normalisation stages (see [online documentation](https://gea.esac.esa.int/archive/documentation/GDR3/Data_analysis/chap_cu8par/sec_cu8par_apsis/ssec_cu8par_apsis_oa.html#SSS3.P1)) and the template that best matches with the prototype. Both the template and the source can be selected through the drop-down menus on the left side.

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/internal_spectra.png" alt="spectra"  class="center-resize"/>


- **Gaia spectra**: It shows the spectra for the best and worst 20 objects of the neuron. These spectra have been crafted using the coefficients available in the [datalink](https://www.cosmos.esa.int/web/gaia-users/archive/datalink-products) and using the [GaiaXPy](https://pypi.org/project/GaiaXPy/) Python tool.

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/external_spectra.png" alt="spectra"  class="center-resize"/>

The following is an example of the code that has been used to craft the spectra:
<pre><code>from gaiaxpy import calibrate
source_ids = [4658089944613959296, 5961472984048330624, 5253665775277326208]
calibrated_spectra, sampling = calibrate(source_ids)
</code></pre>

- **Population**: If available, it shows one pie chart representing the population for each template and catalogue available on the neuron.

<!--<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/population.png" alt="population"  class="center-resize"/>-->

- **Statistical summary**: It shows the statistical summary of all available parameters for this neuron. The description of the parameters is available in the [online documentation](https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_astrophysical_parameter_tables/ssec_dm_oa_neuron_information.html). 

<hr width="50%">

<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/summary.png" alt="summary"  class="center-resize"/>
<h4>Download</h4>
 
 
The Gaia identification of the sources that belong to the selected neurons can be downloaded in fits format by clicking in the "Prepare Download" button. A zip compressed file is downloaded containing one fits file per selected neuron.
 
<br/>
 
<img src="https://gitlab.citic.udc.es/publico/guasom-markdown/-/raw/main/images/download.png" width="20%" alt="download and ADQL buttons"/>
 
<h4>ADQL</h4>
 
Furthermore, with the selected neurons, GUASOM prepares a pair of ADQL queries that can be used to retrieve the belonging sources or the information of the selected neurons directly from the [Gaia Archive](https://gea.esac.esa.int/archive). Just copy and paste the query into the text box in the Advanced (ADQL) tab.

Clicking on "ADL (Source ID)" button, it copies into the clipboard an ADQL query like this:
<pre><code>SELECT a.source_id
FROM gaiadr3.oa_neuron_information n JOIN gaiadr3.astrophysical_parameters a ON n.neuron_id = a.neuron_oa_id
WHERE n.neuron_id in (202105281205440212,202105281205440181);
</code></pre>

Clicking on "ADL (Parameters)" button, it copies into the clipboard an ADQL query like this:
<pre><code>SELECT *
FROM gaiadr3.oa_neuron_information
WHERE neuron_id in (202105281205440212,202105281205440181,202105281205440151,202105281205440302,202105281205440243);
</code></pre>

In both examples, "_(202105281205440212,202105281205440181)_" represents the list of identifiers of the selected neurons, in this case two neurons have been selected.

Check the [online documentation](https://gea.esac.esa.int/archive/documentation/GDR3/Data_analysis/chap_cu8par/sec_cu8par_apsis/ssec_cu8par_apsis_oa.html) for other query examples that can be make with the OA data.
</div>
